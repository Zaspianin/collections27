package pl.sda.collections.rejestr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RejestrObywateli {
    private Map<String, Obywatel> obywatelMap = new HashMap<>();

    public void dodajObywatela(String imie, String nazwisko, String pesel) {
        obywatelMap.put(pesel, new Obywatel(imie, nazwisko, pesel));
    }

    public List<Obywatel> znajdzUrodzonychPrzed(int rok) {
        List<Obywatel> zbiorUrPrzed = new ArrayList<>();
        for (Obywatel obywatel : obywatelMap.values()) {
            String dwieCyfryPeselu = obywatel.getPesel().substring(0, 2);
            int rokZPeselu = Integer.parseInt(dwieCyfryPeselu);
            if (rokZPeselu < rok) {
                zbiorUrPrzed.add(obywatel);
            }
        }
        return zbiorUrPrzed;
//        public List<Obywatel> znajdzUrodzonychPrzed_java8(int rok){
//            return obywatelMap.values().stream()
//                    .filter(o -> o.podajRokUrodzenia() < rok)
//                    .collect(Collectors.toList());
//        }
//        public List<Obywatel> znajdzUrodzonychPo_java8(int rok){
//            return obywatelMap.values().stream()
//                    .filter(o -> o.podajRokUrodzenia() > rok)
//                    .collect(Collectors.toList());
//        }
    }
}

