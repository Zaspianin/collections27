package pl.sda.collections.rejestr;

public class Obywatel {
    private String imie;
    private String nazwisko;
    private String pesel;

    public Obywatel(String imie, String nazwisko, String pesel) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return "Obywatel{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", pesel='" + pesel + '\'' +
                '}';
    }

    public int podajRokUrodzenia() {
        String dwieCyfryPeselu = getPesel().substring(0, 2);
        int rokZPeselu=Integer.parseInt(dwieCyfryPeselu);
        return rokZPeselu;
    }
}
