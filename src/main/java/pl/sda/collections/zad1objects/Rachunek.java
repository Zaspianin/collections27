package pl.sda.collections.zad1objects;

import java.util.ArrayList;
import java.util.List;

public class Rachunek {
    private List<Product> productList = new ArrayList<>();

    public Rachunek(List<Product> productList) {
        this.productList = productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void wypiszWszystkieProdukty() {
        productList.forEach(product -> System.out.println(product));
    }

    public double podsumujRachunekNetto() {

        return productList.stream().mapToDouble(p -> p.getCenaNetto()).sum();

    }
    public double podsumujRachunekBrutto() {

        return productList.stream().mapToDouble(p -> p.getCenaBrutto()).sum();


    }
}
