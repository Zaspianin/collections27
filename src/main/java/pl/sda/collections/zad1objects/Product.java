package pl.sda.collections.zad1objects;

public class Product {
    private PodatekProduktu podatekProduktu;
    private String nazwaProduktu;
    private double cenaNetto;

    public Product(PodatekProduktu podatekProduktu, String nazwaProduktu, double cenaNetto) {
        this.podatekProduktu = podatekProduktu;
        this.nazwaProduktu = nazwaProduktu;
        this.cenaNetto = cenaNetto;
    }

    public PodatekProduktu getPodatekProduktu() {
        return podatekProduktu;
    }

    public String getNazwaProduktu() {
        return nazwaProduktu;
    }

    public double getCenaNetto() {
        return cenaNetto;
    }

    public void setPodatekProduktu(PodatekProduktu podatekProduktu) {
        this.podatekProduktu = podatekProduktu;
    }

    public void setNazwaProduktu(String nazwaProduktu) {
        this.nazwaProduktu = nazwaProduktu;
    }

    public void setCenaNetto(double cenaNetto) {
        this.cenaNetto = cenaNetto;
    }

    public double podajCeneBrutto() {
        double procentPodatku = podatekProduktu.getIloscPodatku() / 100;
        double brutto = cenaNetto + cenaNetto * procentPodatku;
        return brutto;
    }

    @Override
    public String toString() {
        return "Product{" +
                "podatekProduktu=" + podatekProduktu +
                ", nazwaProduktu='" + nazwaProduktu + '\'' +
                ", cenaNetto=" + cenaNetto +
                '}';
    }

    public double getCenaBrutto() {
        return 0;
    }
}
