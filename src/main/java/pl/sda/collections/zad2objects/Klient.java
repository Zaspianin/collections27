package pl.sda.collections.zad2objects;

import java.time.LocalDateTime;

public class Klient {
    String imie;
    LocalDateTime czas;
    boolean priorytet;

    public Klient(String imie, LocalDateTime czas, boolean priorytet) {
        this.imie = imie;
        this.czas = czas;
        this.priorytet = priorytet;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public LocalDateTime getCzas() {
        return czas;
    }

    public void setCzas(LocalDateTime czas) {
        this.czas = czas;
    }

    public boolean isPriorytet() {
        return priorytet;
    }

    public void setPriorytet(boolean priorytet) {
        this.priorytet = priorytet;
    }
}
