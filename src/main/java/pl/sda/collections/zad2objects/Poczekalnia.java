package pl.sda.collections.zad2objects;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Poczekalnia {
    private PriorityQueue<Klient>kolejka;

    public Poczekalnia(){
        this.kolejka=new PriorityQueue<>(new Comparator<Klient>() {
            @Override
            public int compare(Klient o1, Klient o2) {
                if (o1.getCzas().isAfter(o2.getCzas())){
                    return -1;
                }else if(o2.getCzas().isAfter(o1.getCzas())){
                    return 1;
                }
                return 0;
            }
        });
    }
    public void dodajKlienta(String imie,boolean czyPriorytet){
        kolejka.add(new Klient(imie,LocalDateTime.now(),czyPriorytet));
    }
//    public Klient pobierzNastepnegoKlienta(){
//        return kolejka
//    }
}
