package pl.sda.collections.zbiory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class MainZbiory {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55));
        //System.out.println(set.size());
        //set.forEach(set0-> System.out.println(set0));
        Set<Integer> set3 = new TreeSet<>(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55));
        //System.out.println(set3.size());
        set3.forEach(element -> System.out.println(element));


        // Set<Integer> set2 = new LinkedHashSet<>();         //zachowuje kolejność wstawiania
        // Set<Integer> set3 = new TreeSet<>();               //po kluczach
    }}
