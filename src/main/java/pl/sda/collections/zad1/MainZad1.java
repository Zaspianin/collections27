package pl.sda.collections.zad1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class MainZad1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i <5; i++){
            int zmienna = scanner.nextInt();
            list.add(zmienna);
        }
        Random generator = new Random();
        for (int i = 0; i <5; i++){
            int zmienna = generator.nextInt();
            list.add(zmienna);

            System.out.println("pokaż listę: " + list);
        }
    }
}
