package pl.sda.collections.streams.predicates;

import java.util.*;
import java.util.stream.Collectors;

public class MainPredicates {
    public static void main(String[] args) {
//        Predicate<Integer> podzielna = l -> l % 2 == 0;
//        Predicate<Integer> podzielnaDlugie = new Predicate<Integer> {
//        }
//        ;
//        Predicate<List<String>> predicate = new Predicate<List<String>>() {
//            @Override
//            public boolean test(List<String> strings) {
//                for (String tekt : strings) {
//                    if (tekt.contains("gotchya")) {
//                        {
//                            return true;
//                        }
//                    }
//                    return false;
//
//                }
//            }
//
//            Predicate<Person> personPredicate = p.->p.isMale();
//            Predicate<Person>personPredicate1=p.->p.getFirst
//
//        }
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);
        List<Person> personList = new ArrayList<>(Arrays.asList(

                person1,

                person2,

                person3,

                person4,

                person5,

                person6,

                person7,

                person8,

                person9

        ));
        List<Person> mezczyzni = personList.stream()
                .filter(p -> p.isMale())
                .collect(Collectors.toList());
        List<Person> kobieta_dorosle = personList.stream()
                .filter(p -> p.getAge() > 18)
                .filter(p -> !p.isMale())
                .collect(Collectors.toList());
        Optional<Person> dorosly_Jacek = personList.stream()
                .filter(p -> p.getFirstName().equals("Jacek"))
                .filter(p -> p.getAge() > 18)
                .findFirst();
        List<String> listaNazwisk = personList.stream()
                .filter(p -> p.getAge() > 15 && p.getAge() < 19)
                .map(p -> p.getLastName())
                .collect(Collectors.toList());
        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");

        Programmer programmer1 = new Programmer(person1, languages1);
        Programmer programmer2 = new Programmer(person2, languages2);
        Programmer programmer3 = new Programmer(person3, languages3);
        Programmer programmer4 = new Programmer(person4, languages4);
        Programmer programmer5 = new Programmer(person5, languages5);
        Programmer programmer6 = new Programmer(person6, languages6);
        Programmer programmer7 = new Programmer(person7, languages7);
        Programmer programmer8 = new Programmer(person8, languages8);
        Programmer programmer9 = new Programmer(person9, languages9);

        List<Programmer> programmers = Arrays.asList(programmer1, programmer2, programmer3, programmer4,
                programmer5, programmer6, programmer7, programmer8, programmer9);
        // uzyskaj listę programistów, którzy są mężczyznami
        List<Programmer> mezczyzni_prog = programmers.stream()
                .filter(p -> p.getPerson().isMale())
                .collect(Collectors.toList());
        System.out.println(mezczyzni_prog);
        // uzyskaj listę niepełnoletnich programistów (obydwóch płci), którzy piszą w Cobolu
        List<Programmer> age_18 = programmers.stream()
                .filter(p -> p.getPerson().getAge() < 18)
                .filter(p -> p.getLanguages().contains("Cobol"))
                .collect(Collectors.toList());
        System.out.println(age_18);
        //uzyskaj listę programistów, którzy znają więcej, niż jeden język programowania
        List<Programmer> lang_prog = programmers.stream()
                .filter(p -> p.getLanguages().size() > 2)
                .collect(Collectors.toList());
        System.out.println(lang_prog);
        //uzyskaj listę programistek, które piszą w Javie i Cpp
        List<Programmer> kobiet_Java_Cpp = programmers.stream()
                .filter(p -> !p.getPerson().isMale())
                .filter(p -> p.getLanguages().containsAll(Arrays.asList("Java;Cpp".split(";"))))
                .collect(Collectors.toList());
        System.out.println(kobiet_Java_Cpp);
        //uzyskaj listę męskich imion
        List<String> imiona_meskie = programmers.stream ()
                .filter(p -> p.getPerson().isMale())
                .map(p->p.getPerson().getFirstName())
                .collect(Collectors.toList());
        System.out.println(imiona_meskie);
        //uzyskaj set wszystkich języków opanowanych przez programistów
        List<Programmer>all_lang=programmers.stream()
                .filter(p->p.getLanguages().containsAll(Arrays.asList("".split(";"))))
                .collect(Collectors.toList());
        System.out.println(all_lang);
        //uzyskaj listę nazwisk programistów, którzy znają więcej, niż dwa języki
        List<String>prog_more_2_lang=programmers.stream()
                .filter(p->p.getLanguages().size()>2)
                .map(p->p.getPerson().getLastName())
                .collect(Collectors.toList());
        System.out.println(prog_more_2_lang);

    }
}

