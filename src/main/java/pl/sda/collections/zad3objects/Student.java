package pl.sda.collections.zad3objects;

public class Student {
    private String nazwisko, imie;
    private Long indeks;

    public Student(String nazwisko, String imie, Long indeks) {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.indeks = indeks;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public Long getIndeks() {
        return indeks;
    }

    public void setIndeks(Long indeks) {
        this.indeks = indeks;
    }

    @Override
    public String toString() {
        return "Student{" +
                "nazwisko='" + nazwisko + '\'' +
                ", imie='" + imie + '\'' +
                ", indeks=" + indeks +
                '}';
    }
}
