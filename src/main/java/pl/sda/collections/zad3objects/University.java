package pl.sda.collections.zad3objects;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class University {
    private Map<Long, Student> studentMap = new HashMap<>();

    public void dodajStudenta(String nazwisko, String imie, Long indeks) {
        Student stworzony = new Student(nazwisko, imie, indeks);
        if (studentMap.containsKey(indeks)) {
            System.err.println("Student z takim ideksem już istnieje.");
            while (studentMap.containsKey(indeks)) {
                indeks++;
            }
        }
        stworzony = new Student(nazwisko, imie, indeks);
        studentMap.put((stworzony.getIndeks()), stworzony);
    }

    public boolean czyStudentIstnieje(Long indeks) {
        return studentMap.containsKey(indeks);
    }

    public Optional<Student> pobierzStudenta(Long indeks) {
        return Optional.ofNullable(studentMap.get(indeks));
    }

    public int pobierzIlośćStudentów() {
        return studentMap.size();
    }

    public int pobierzUnikalnychStudentów() {
        return (int) studentMap.values().stream().distinct().count();
    }

    public void printAllStudents() {
        studentMap.values().stream()
                .distinct()
                .forEach(System.out::println);
    }

}

