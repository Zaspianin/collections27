package pl.sda.collections.zad3objects;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class MainZad3 {
    public static void main(String[] args) {
        Map<Long, Student> map = new HashMap<>();
        map.put(222L, new Student("Mariusz", "Adamkiewicz", 222L));
        map.put(333L, new Student("Zdzisław", "Nowacki", 333L));
        map.put(441L, new Student("Norbert", "Iwański", 441L));
        map.put(552L, new Student("Sławomir", "Wzakopanym", 552L));
        map.put(664L, new Student("Adam", "Słodowy", 664L));
        map.put(734L, new Student("Alex", "Kwaśniewski", 734L));
        if (map.containsKey(222L)) {
            System.out.println("zawiera");
            System.out.println("Student: " + map.get(222L));
        } else {
            System.out.println("Nie zawiera");
        }
        System.out.println("Ilość studentów: " + map.size());

        for (Long kluczWMapie : map.keySet()) {
            System.out.println("Klucz: " + kluczWMapie);
        }

        for (Student student : map.values()) {//wypisuje wszytskich studentów
            System.out.println(student);
        }
        for (Map.Entry<Long, Student> wpis:map.entrySet()){
            System.out.println("Klucz: "+wpis.getKey()+",wartość: "+wpis.getValue());
        }
    }

}
